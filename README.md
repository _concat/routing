[![Author](http://img.shields.io/badge/author-@rudi_theunissen-blue.svg?style=flat)](https://twitter.com/rudi_theunissen)
[![Software License](https://img.shields.io/packagist/l/concat/routing.svg?style=flat)](LICENSE.md)
[![Build Status](https://img.shields.io/travis/concat/routing/master.svg?style=flat)](https://travis-ci.org/concat/routing)
[![Scrutinizer Code Quality](https://img.shields.io/scrutinizer/g/concat/routing.svg?style=flat)](https://scrutinizer-ci.com/g/concat/routing/?branch=master)
[![Code Coverage](https://img.shields.io/scrutinizer/coverage/g/concat/routing.svg?style=flat)](https://scrutinizer-ci.com/g/concat/routing/?branch=master)
[![Version](https://img.shields.io/packagist/v/concat/routing.svg?style=flat)](https://packagist.org/packages/concat/routing)


## Install

Via Composer

``` json
require: {
    "concat/routing": "dev-master"
}
```
