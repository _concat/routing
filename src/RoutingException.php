<?php

namespace Concat\Routing;

/**
 * Determines how route destinations are handled.
 *
 * @author      Rudi Theunissen <rudolf.theunissen@gmail.com>
 * @link        https://github.com/concat/routing Github
 * @license     MIT
 */
class RoutingException extends \Exception
{
    private $route;

    public function __construct($message, Route $route)
    {
        parent::__construct($message);

        $this->route = $route;
    }


    public function getRoute()
    {
        return $this->route;
    }
}
