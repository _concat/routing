<?php

namespace Concat\Routing;

/**
 * Responsible for matching a URL path to a route.
 *
 * @author      Rudi Theunissen <rudolf.theunissen@gmail.com>
 * @link        https://github.com/concat/routing Github
 * @license     MIT
 */
class Matcher implements RouteMatcher
{
    /**
     * Used to store the request URL path.
     *
     * @var string
     */
    private $path;

    /**
     * Used to store URL path parts, exploded by '/'.
     *
     * @var array
     */
    private $levels;

    /**
     * Used to keep track of the current route path level.
     *
     * @var int
     */
    private $current;

    /**
     * Stack for collecting captured placeholder values
     *
     * @var array
     */
    private $values;

    /**
     * Stack for collecting placeholder patterns
     *
     * @var array
     */
    private $patterns;


    /**
     * Attempts to find a match in a set of routes for a given URL path.
     * The key determines which of the matching route's destinations
     * to be returned, and defaults to a non-specific key.
     *
     * @param array  $routes the routes to check for a match
     * @param string $path   the request url path to match against
     * @param mixed  $key    the destination key, eg GET
     *
     * @return array<mixed>|null destination and captured values
     */
    public function match(array $routes, $path, $key = '*')
    {
        $this->setup($path);

        foreach ($routes as $route) {

            // we need to start from scratch for each base level route
            $this->reset();

            // this will be a route if the match was successful
            $match = $this->matchRoute($route);

            if ($match && $this->validate()) {
                return $this->success($match, $key);
            }
        }
    }


    /**
     * Normalises the request URL path and sets its levels.
     *
     * @param string $path the request URL path
     */
    private function setup($path)
    {
        // trim leading and trailing edges to normalise the path
        $this->path = trim($path, '/');

        // set the request to be an array of each level of the url.
        // eg. /match/this/path, request will be [match, this, path]
        $this->levels = explode('/', $this->path);
    }


    /**
     * Resets the collecting properties to start a new route match from scratch.
     */
    private function reset()
    {
        $this->current  = 0;
        $this->values   = [];
        $this->patterns = [];
    }


    /**
     * Attempts to match a route against the current state of the url path.
     *
     * @param Route $route the route to match against the request
     *
     * @return Route|null the matching route, null if no match
     */
    private function matchRoute(Route $route)
    {
        // trim leading and trailing slashes to improve path tolerance
        $path = trim($route->getPath(), '/');

        return $this->matchPath($path, $route);
    }


    /**
     * Checks if there are still path levels to match against the route's
     * subroutes, or returns the route if no path levels are left.
     *
     * @param Route $route the route that has matched up to this point.
     *
     * @return Route|null the matching route, null of no match
     */
    private function matchRemainder(Route $route)
    {
        // check to see if there are still url path levels remaining
        if (isset($this->levels[$this->current])) {
            return $this->matchSubroutes($route);
        }

        // if no more parts left or level is blank, we have a match.
        return $route;
    }


    /**
     * Attempts to match each of a route's subroutes against the remainder
     * of the request URL path.
     *
     * @param Route $route the route which subroutes to match
     *
     * @return Route|null matching route if successful, null otherwise
     */
    private function matchSubroutes(Route $route)
    {
        foreach ($route->getSubroutes() as $subroute) {

            // recursive call to match subroute
            if ($route = $this->matchRoute($subroute)) {
                return $route;
            }
        }
    }


    /**
     * Attempts to match a specific path to a specific route.
     *
     * @param string $path the path to match against the route
     * @param Route  $route the route to match against the path
     *
     * @return Route|null the matching route, null if no match
     */
    private function matchPath($path, Route $route)
    {
        // match if the path is a literal string match
        if ($path === $this->path) {
            return $route;
        }

        // push the route's patterns onto the stack
        $this->patterns += $route->getPatterns();

        // not a literal match so match path levels
        if ($this->matchPathLevels($path) !== false) {
            return $this->matchRemainder($route);
        }
    }


    /**
     * Matches each level of the given path against the request URL path.
     *
     * @param string $path the path to match against the request URL path.
     *
     * @return false|null false if a level doesn't match, null otherwise
     */
    private function matchPathLevels($path)
    {
        $level  = strtok($path, '/');

        while ($level !== false) {

            // fail if the level doesn't match
            if (!$this->matchLevel($level)) {
                return false;
            }

            // move on to the next level of the request url
            $this->current++;

            // move on to the next level in the route path
            $level = strtok('/');
        }
    }


    /**
     * Tries to match a route path level against the current request URL level.
     *
     * @param string $level the route path level
     *
     * @return boolean true if the level is was valid, false otherwise
     */
    private function matchLevel($level)
    {
        $value = $this->levels[$this->current];

        // check if the level is a placeholder
        if ($level[0] === ":") {

            // collect the placeholder value to be validated later
            $this->values[substr($level, 1)] = $value;

            return true;
        }

        // match static level literally
        return $level === $value;
    }


    /**
     * Checks if patterns were collected and validates them if there were.
     */
    private function validate()
    {
        return !$this->patterns || $this->validatePatterns();
    }


    /**
     * Validates each collected pattern against their corresponding value.
     *
     * @return boolean true if all values validate, false otherwise
     */
    private function validatePatterns()
    {
        $regex = "";
        $value = "";

        // loop through collected patterns to build the regex
        foreach ($this->patterns as $key => $pattern) {
            $regex .= $pattern;
            $value .= $this->values[$key];
        }

        // check if all the values match their corresponding values
        return (bool) preg_match("~$regex~", $value);
    }


    /**
     * Determines which of a route's destinations to resolve to, given
     * a specific route destination key.
     *
     * @param Route $route the matching route
     * @param mixed $key the route destination key
     *
     * @throws InvalidKeyException if the specified key was not registered
     */
    private function success(Route $route, $key)
    {
        // get the destination set for the request key
        if ($destination = $route->getDestination($key)) {

            $values = $this->values;

            // valid destination key, return matched destination
            return compact('destination', 'values');
        }

        // match was valid but destination key does not match
        throw new InvalidKeyException($route, $key);
    }
}
