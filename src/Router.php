<?php

namespace Concat\Routing;

/**
 * This is the main interface to the routing library, and would
 * be the object you use to delegate dispatching and reversing.
 *
 * @author      Rudi Theunissen <rudolf.theunissen@gmail.com>
 * @link        https://github.com/concat/routing Github
 * @license     MIT
 */
class Router
{
    // these routes are expected to conform to a specific structure.
    // see the routes in the tests for an example

    private $routes = [];

    // having a dispatcher provided keeps the dispatching behaviour separate from route resolving
    private $dispatcher;

    //
    private $matcher;

    //
    private $reverser;

    public function __construct($routes = [])
    {
        $this->routes = $routes;
    }

    public function setRoutes(array $routes)
    {
        $this->routes = $routes;
    }

    public function addRoute(Route $route)
    {
        $this->routes[] = $route;
    }

    public function setMatcher(RouteMatcher $matcher)
    {
        $this->matcher = $matcher;
    }

    public function setReverser(RouteReverser $reverser)
    {
        $this->reverser = $reverser;
    }

    public function setDispatcher(RouteDispatcher $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    public function dispatch($path, $key = "*")
    {
        if ($this->dispatcher === null) {
            $this->setDispatcher(new Dispatcher());
        }

        // key value pair of destination and values extracted from request path
        if ($resolved = $this->match($path, $key)) {
            extract($resolved);

            // delegate to dispatcher
            return $this->dispatcher->dispatch($destination, $values);
        }
    }

    public function reverse($name, $values = [])
    {
        if ($this->reverser === null) {
            $this->setReverser(new Reverser());
        }

        return $this->reverser->reverse($this->routes, $name, $values);
    }

    public function match($path, $key = "*")
    {
        if ($this->matcher === null) {
            $this->setMatcher(new Matcher());
        }

        return $this->matcher->match($this->routes, $path, $key);
    }
}
