<?php

namespace Concat\Routing;

/**
 * Determines how route destinations are handled.
 *
 * @author      Rudi Theunissen <rudolf.theunissen@gmail.com>
 * @link        https://github.com/concat/routing Github
 * @license     MIT
 */
interface RouteMatcher
{
    public function match(array $routes, $uri, $key);
}
