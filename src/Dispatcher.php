<?php

namespace Concat\Routing;

/**
 * Determines how route destinations are handled.
 *
 * @author      Rudi Theunissen <rudolf.theunissen@gmail.com>
 * @link        https://github.com/concat/routing Github
 * @license     MIT
 */
class Dispatcher implements RouteDispatcher
{
    /**
     * Handles the dispatch of a destination and values extracted from the request
     *
     * @param destination a route endpoint defined in the route spec
     * @param values placeholder values extracted from the request uri
     */
    public function dispatch($destination, $values, ...$args)
    {
        if (is_callable($destination)) {
            // resolved destination is callable so we can just invoke it
            return $this->dispatchCallable($destination, $values, $args);
        }

        if (is_object($destination)) {
            return $this->dispatchObject($destination, $values, $args);
        }

        // not callable
        // no object with dispatch method
        // for now just return the resolved data
        return $this->dispatchFallback($destination, $values, $args);
    }

    private function dispatchFallback($destination, $values, $args)
    {
        return compact('destination', 'values', 'args');
    }

    private function dispatchObject($destination, $values, $args)
    {
        if (method_exists($destination, 'dispatch')) {
            return $destination->dispatch($values, ...$args);
        }

        return $this->dispatchFallback($destination, $values, $args);
    }

    private function dispatchCallable(callable $destination, $values, $args)
    {
        $result = $destination($values, ...$args);

        if (is_object($result)) {
            return $this->dispatchObject($values, ...$args);
        }

        return $result;
    }
}
