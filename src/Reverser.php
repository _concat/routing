<?php

namespace Concat\Routing;

/**
 * Takes a route name and optional key/value pairs and builds
 * the corresponding URL, injecting the values into the placeholders.
 *
 * @author      Rudi Theunissen <rudolf.theunissen@gmail.com>
 * @link        https://github.com/concat/routing Github
 * @license     MIT
 */
class Reverser implements RouteReverser
{
    /**
     * Does a name-based lookup on the routes to return a complete path.
     * Also injects passed values into the path's placeholders
     */
    public function reverse($routes, $name, $values)
    {
        // find the path in the routes which corresponds to the given name
        if (!($lookup = $this->lookup($routes, $name))) {
            throw new InvalidNameException($name);
        }

        list($path, $patterns) = $lookup;

        if ($patterns) {
            // return the path after injecting values into the placeholders
            $path = $this->inject($path, $patterns, $values);
        }

        return $path;
    }

    private function validateValue($key, $value, $patterns)
    {
        if (isset($patterns[$key]) && !preg_match("~$patterns[$key]~", $value)) {

            //
            throw new RouteException("'$key' does not match '$patterns[$key]'");
        }
    }

    private function validateValues($patterns, $values)
    {
        if ($diff = array_diff_key($patterns, $values)) {
            // this means that there is a key mismatch
            throw new RouteException(
                "Value not provided for: ".join(", ", array_keys($diff))
            );
        }

        foreach ($values as $key => $value) {
            $this->validateValue($key, $value, $patterns);
        }
    }

    private function getReplacements($values)
    {
        $keys = [];

        foreach ($values as $key => $value) {
            $keys[] = ":$key";
        }

        return [$keys, $values];
    }

    private function inject($path, $patterns, $values)
    {
        $this->validateValues($patterns, $values);

        list($search, $replace) = $this->getReplacements($values);

        // do the actual replacement of placeholders with corresponding values
        return str_replace($search, $replace, $path);
    }

    private function getRemainder($route, $search)
    {
        if ($name = $route->getName()) {
            // this checks if the search term starts with this route's name
            if (substr($search, 0, strlen($name)) === $name) {
                // the +1 here is to advance past the name delimiter, :
                return substr($search, strlen($name) + 1);
            }
        } else {
            return $search;
        }

        return $search;
    }

    private function lookupSubroutes($route, $remainder)
    {
        if ($lookup = $this->lookup($route->getSubroutes(), $remainder)) {
            list($path, $patterns) = $lookup;

            $path = trim($route->getPath()."/".$path, "/");

            $patterns = array_merge($route->getPatterns(), $patterns);

            return [$path, $patterns];
        }
    }

    private function lookupRoute($route, $search)
    {
        if ($remainder = $this->getRemainder($route, $search)) {
            //
            if ($lookup = $this->lookupSubroutes($route, $remainder)) {
                return $lookup;
            }
        } elseif ($remainder !== null) {
            // partial match with no remainder left means we have a complete match
            return [$route->getPath(), $route->getPatterns()];
        }
    }

    // returns an array of routes, or just one
    private function lookup($routes, $search)
    {
        // go through all the routes to find one defined by the search name
        foreach ($routes as $route) {
            //
            if ($lookup = $this->lookupRoute($route, $search)) {
                return $lookup;
            }
        }
    }
}
