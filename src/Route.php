<?php

namespace Concat\Routing;

/**
 * A container for a basic route definition.
 *
 * @author      Rudi Theunissen <rudolf.theunissen@gmail.com>
 * @link        https://github.com/concat/routing Github
 * @license     MIT
 */
class Route
{
    private $path;
    private $name;
    private $subroutes = [];

    private $patterns = [];

    private $destinations = [];

    public function __construct($path, $patterns = [])
    {
        $this->path = $path;
        $this->patterns = $patterns;
    }

    public function getPatterns()
    {
        return $this->patterns;
    }

    public function setPattern($name, $pattern)
    {
        $this->patterns[$name] = $pattern;

        return $this;
    }

    public function setPatterns(array $patterns)
    {
        $this->patterns = $patterns;

        return $this;
    }

    public function append($subroutes)
    {
        $this->subroutes = array_merge($this->subroutes, $subroutes);

        return $this;
    }

    public function to($destination, ...$keys)
    {
        if ($keys) {
            foreach ($keys as $key) {
                $this->destinations[$key] = $destination;
            }
        } else {
            $this->destinations['*'] = $destination;
        }

        return $this;
    }

    public function name($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getSubroutes()
    {
        return $this->subroutes;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function getDestination($key)
    {
        if (isset($this->destinations[$key])) {
            return $this->destinations[$key];
        }
    }

    public function getName()
    {
        return $this->name;
    }
}
