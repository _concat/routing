<?php

namespace Concat\Routing;

/**
 * Determines how route destinations are handled.
 *
 * @author      Rudi Theunissen <rudolf.theunissen@gmail.com>
 * @link        https://github.com/concat/routing Github
 * @license     MIT
 */
class InvalidNameException extends \Exception
{
    private $name;

    public function __construct($name)
    {
        parent::__construct("Could not reverse '$name'");

        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }
}
