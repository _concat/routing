<?php

namespace Concat\Routing;

/**
 * Determines how route destinations are handled.
 *
 * @author      Rudi Theunissen <rudolf.theunissen@gmail.com>
 * @link        https://github.com/concat/routing Github
 * @license     MIT
 */
class InvalidKeyException extends RoutingException
{
    private $key;

    public function __construct(Route $route, $key)
    {
        parent::__construct("Key not registered as valid destination", $route);

        $this->key = $key;
    }

    public function getKey()
    {
        return $this->key;
    }
}
