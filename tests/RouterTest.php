<?php

namespace Concat\Routing\Tests;

use Concat\Routing\Router;
use Concat\Routing\RouteException;

class RouterTest extends \PHPUnit_Framework_TestCase
{
    private $router;

    private function getRoutes()
    {
        return require __DIR__."/fixtures/routes.php";
    }

    protected function setUp()
    {
        $this->router = new Router($this->getRoutes());
    }

    protected function tearDown()
    {
        $this->router = null;
    }

    public function testSetRoutes()
    {
        $router = new Router();
        $router->setRoutes($this->getRoutes());
        $this->assertNotNull(
            $this->router->match(""),
            $this->router->match("dashboard"),
            $this->router->match("search/50")
        );
    }

    public function testAddRoutes()
    {
        $router = new Router();
        foreach ($this->getRoutes() as $route) {
            $router->addRoute($route);
        }
        $this->assertNotNull(
            $this->router->match(""),
            $this->router->match("dashboard"),
            $this->router->match("search/50")
        );
    }

    public function testDispatch()
    {
        $result = $this->router->dispatch("search/50");

        $destination = 'Search';
        $values = ['id' => 50];

        $this->assertEquals(compact('destination', 'values'), $result);
    }

    public function testDispatchClosure()
    {
        $result = $this->router->dispatch("closure/10");

        $this->assertEquals(['arg' => 10], $result);
    }

    public function testBadDispatch()
    {
        $result = $this->router->dispatch("invalid");

        $this->assertNull($result);
    }

    public function testMatch()
    {
        extract($this->router->match("search/50"));

        $this->assertEquals('Search', $destination);
        $this->assertEquals(['id' => 50], $values);
    }

    public function testMatchMultiplePlaceholders()
    {
        extract($this->router->match("search/50/text"));

        $this->assertEquals('Advanced', $destination);
        $this->assertEquals(['id' => 50, 'name' => 'text'], $values);
    }

    private function throws($message, callable $function)
    {
        try {
            $function();
        } catch (RouteException $e) {
            $this->assertEquals($message, $e->getMessage());
        }
    }

    public function testMisMatch()
    {
        $this->assertNull($this->router->match("path/not/specified"));
    }

    public function testReverse()
    {
        $url = $this->router->reverse('search', ['id' => 50]);
        $this->assertEquals('search/50', $url);
    }

    public function testReverseBadName()
    {
        $this->throws("'invalid' not recognised as valid route", function () {
            $this->router->reverse('invalid');
        });
    }

    public function testReverseBadValue()
    {
        $this->throws("'id' does not match '\d{2}'", function () {
            $this->router->reverse('search', ['id' => 'string']);
        });
    }
}
