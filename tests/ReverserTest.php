<?php

namespace Concat\Routing\Tests;

use Concat\Routing\Reverser;

class ReverserTest extends \PHPUnit_Framework_TestCase
{
    // use RouterTest;

    protected function setUp()
    {
        // $this->

        // $this->router = new Router($this->routes);
    }

    private function reverse($expected, $name, $values = [])
    {
        $routes = require "fixtures/routes.php";
        // var_dump($routes);
        // die();
        $actual = (new Reverser())->reverse($routes, $name, $values);
        $this->assertEquals($expected, $actual);
    }

    private function throws($message, callable $function)
    {
        try {
            $function();
        } catch (\Exception $e) {
            $this->assertEquals($message, $e->getMessage());
        }
    }

    public function testReversePlaceholder()
    {
        $this->reverse('search/50', 'search', ['id' => 50]);
    }

    public function testReverseBadPlaceholder()
    {
        $this->throws("'id' does not match '\d{2}'", function () {
            $this->reverse('search/50', 'search', ['id' => 'NaN']);
        });
    }

    public function testReverseMissingPlaceholder()
    {
        $this->throws("Value not provided for: id", function () {
            $this->reverse('search/50', 'search');
        });
    }

    public function testReverseRoot()
    {
        $this->reverse('', 'home');
    }

    public function testReverseSubroute()
    {
        $this->reverse('dashboard', 'home:dash');
    }

    public function testReverseWhereKeysDefined()
    {
        $this->reverse('explicit', 'explicit');
    }

    public function testReverseMultiplePlaceholders()
    {
        $this->reverse('search/50/test', 'search:advanced', ['id' => 50, 'name' => 'test']);
    }

    public function testReverseBlankName()
    {
        $this->throws("'' not recognised as valid route", function () {
            $this->reverse(null, '');
        });
    }

    public function testReverseNameWithoutParent()
    {
        $this->reverse('about/team', 'team');
    }

    public function testReverseLongPath()
    {
        $expected = 'long/path/A/with/123/multiple/patterns/ABC/tail/end/xyz';

        $this->reverse($expected, 'long-path:tail', [
            'a' => 'A',
            'b' => 123,
            'c' => 'ABC',
            'd' => 'xyz'
        ]);
    }

    public function testReverseLongPathWithMissingTailValue()
    {
        $this->throws("Value not provided for: d", function () {
            $this->reverse(null, 'long-path:tail', [
                'a' => 'A',
                'b' => 123,
                'c' => 'ABC'
            ]);
        });
    }

    public function testReverseLongPathWithMissingBaseValues()
    {
        $this->throws("Value not provided for: a, c", function () {
            $this->reverse(null, 'long-path:tail', [
                //'a' => 'A',
                'b' => 123,
                //'c' => 'ABC'
                'd' => 'xyz'
            ]);
        });
    }
}
