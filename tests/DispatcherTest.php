<?php

namespace Concat\Routing\Tests;

use Concat\Routing\Dispatcher;

class DispatcherTest extends \PHPUnit_Framework_TestCase
{
    private function dispatch(...$args)
    {
        return (new Dispatcher())->dispatch(...$args);
    }

    public function testDispatchCallable()
    {
        $callable = function ($values, $arg) {
            return [$values, $arg];
        };

        $result = $this->dispatch($callable, ['key' => 'value'], 'arg');

        $this->assertEquals([['key' => 'value'], 'arg'], $result);
    }

    public function testCallableReturingDispatchable()
    {
        $callable = function () {
            return new Dispatchable();
        };

        $result = $this->dispatch($callable, ['key' => 'value'], 'arg');

        $this->assertEquals([['key' => 'value'], 'arg'], $result);
    }

    public function testCallableReturningNonDispatchable()
    {
        $callable = function () {
            return new \stdClass();
        };

        $result = $this->dispatch($callable, ['key' => 'value']);

        $this->assertEquals(new \stdClass(), $result);
    }

    public function testDispatchable()
    {
        $object = new Dispatchable();

        $result = $this->dispatch($object, ['key' => 'value'], 'arg');

        $this->assertEquals([['key' => 'value'], 'arg'], $result);
    }

    public function testNonDispatchable()
    {
        $object = new \stdClass();

        $destination = $object;
        $values = ['key' => 'value'];
        $expected = compact('destination', 'values');

        $result = $this->dispatch($object, $values);
        $this->assertEquals($expected, $result);
    }
}
