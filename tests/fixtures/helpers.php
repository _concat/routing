<?php

if (!function_exists('route')) {
    function route($path, $patterns = [])
    {
        return new \Concat\Routing\Route($path, $patterns);
    }
}
