<?php

require "helpers.php";

return [

    // note that leading and trailing slashes are optional,
    // so "/about" == "about/" == "/about/"

    // root path
    route('')->to('Home')->name('home')->append([

        // named following no name, should match /dashboard
        route('dashboard')
            ->to('Dashboard')
            ->name('dash'),
    ]),

    // get specific
    route('explicit')->name('explicit')
        // destination, keys
        ->to('Get',     'GET')
        ->to('Post',    'POST')
        ->to('Put',     'PUT')
        ->to('Delete',  'DELETE'),

    // no placeholder route match
    route('about')
        ->to('About')

        // nested route which would resolve as about/team
        ->append([
            route('team')
                ->to('Team')
                ->name('team'),
        ]),

    // named regex placeholder
    route('search/:id')
        ->to('Search')
        ->name('search')
        ->setPattern('id', '\d{2}')

        // braced regex and greedy placeholder
        ->append([
            route(':name')
                ->name("advanced")
                ->to('Advanced'),
        ]),

    route('long/path/:a/with/:b/multiple/patterns/:c')
        ->to("Long")
        ->name("long-path")
        ->setPatterns([
            'a' => '.+',
            'b' => '\d{3}',
            'c' => '[\w]+',
        ])
        ->append([
            route('tail/end/:d')
            ->name('tail')
            ->setPattern('d', '.+'),

        ]),

    // closure destination
    route('closure')->to(function () {
        return 'success';
    }),

    // closure destination with arg
    route('closure/:arg')->to(function ($arg) {
        return $arg;
    }),
];
