<?php

namespace Concat\Routing\Tests;

use Concat\Routing\Matcher;

class MatcherTest extends \PHPUnit_Framework_TestCase
{
    //use RouterTest;

    public function setUp()
    {
        $this->matcher = new Matcher();
    }

    private function throws($message, callable $function)
    {
        try {
            $function();
        } catch (\Exception $e) {
            $this->assertEquals($message, $e->getMessage());
        }
    }

    private function match($uri, ...$args)
    {
        $routes = require "fixtures/routes.php";

        $expected = $this->matcher->match($routes, $uri, ...$args);

        $variations = [
            "/".$uri,
            $uri."/",
           "/".$uri."/",
        ];

        foreach ($variations as $variation) {
            $actual = $this->matcher->match($routes, $variation, ...$args);
            $this->assertEquals($expected, $actual);
        }

        return $expected;
    }

    public function testMatchRoot()
    {
        extract($this->match(""));

        $this->assertEquals("Home", $destination);
        $this->assertEmpty($values);
    }

    public function testMatchInvalidKey()
    {
        $this->throws("Route key mismatch", function () {
            $this->match("explicit", "*");
        });
    }

    public function testMatchSpecificKey()
    {
        extract($this->match("explicit", 'POST'));

        $this->assertEquals("Post", $destination);
        $this->assertEmpty($values);
    }

    public function testMatchPlaceholder()
    {
        extract($this->match("search/50"));

        $this->assertEquals("Search", $destination);
        $this->assertEquals(["id" => 50], $values);
    }

    public function testMatchNestedPlaceholders()
    {
        extract($this->match("search/50/test"));

        $this->assertEquals("Advanced", $destination);
        $this->assertEquals(["id" => 50, "name" => "test"], $values);
    }

    public function testBadPattern()
    {
        $this->assertNull($this->match("search/text"));
    }

    public function testMatchClosure()
    {
        extract($this->match("closure"));

        $this->assertInstanceOf('Closure', $destination);
        $this->assertEmpty($values);
    }

    public function testMismatch()
    {
        $this->assertNull($this->match("path/not/specified"));
    }

    public function testPartialMismatch()
    {
        $this->assertNull($this->match("about/team/member"));
    }
}
