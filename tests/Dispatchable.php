<?php

namespace Concat\Routing\Tests;

class Dispatchable
{
    public function dispatch($values, $arg)
    {
        return [$values, $arg];
    }
}
